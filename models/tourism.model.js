const { DataTypes } = require('sequelize');
const sequelize = require('../config/db');
// Define the User model
const Tourism = sequelize.define('tourism', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    image: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    is_favorite: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    latlong: {
        type: DataTypes.STRING,
        allowNull: true
    },
    invitation_text: {
        type: DataTypes.STRING,
        allowNull: true
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    // You can define more attributes as needed
});


module.exports = Tourism;
