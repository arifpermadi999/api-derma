// login.route.js

const express = require('express');
const Tourism = require('../models/tourism.model');
const { Op } = require('sequelize');

const verifyToken = require('../middleware/verifyToken');
const router = express.Router();

// Login route
router.post('/web/tourism', async (req, res) => {
    try {
        const user = await Tourism.create(req.body);
        res.json({ message: "success create tourism" });
    } catch (error) {
        console.error('Error logging in:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
});
router.get('/web/tourism', async (req, res) => {
    try {
        const { search } = req.query;
        var tourism;
        if (search) {
            tourism = await Tourism.findAll({ where: { name: { [Op.like]: `%${search}%` } } });
        } else {
            tourism = await Tourism.findAll();
        }
        res.json(tourism);
    } catch (error) {
        console.error('Error logging in:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
});

router.post('/web/tourism/setFavorite', async (req, res) => {
    try {
        const { id, favorite } = req.body;
        const tourism = await Tourism.update({ is_favorite: favorite }, { where: { id: id } });
        res.json({ message: "success set favorite" });
    } catch (error) {
        console.error('Error logging in:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
});
router.patch('/web/tourism/:id', async (req, res) => {
    const { id } = req.params;
    try {
        await Tourism.update(req.body, { where: { id: id } });
        res.json({ message: "success update tourism" });
    } catch (error) {
        console.error('Error logging in:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
});

router.delete('/web/tourism/:id', async (req, res) => {
    const { id } = req.params;
    try {
        await Tourism.destroy({ where: { id: id } });
        res.json({ message: "success delete tourism" });
    } catch (error) {
        console.error('Error logging in:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
});

module.exports = router;
