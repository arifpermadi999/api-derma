const express = require('express');
const router = express.Router();
const TourismService = require('../service/tourism.service')
const verifyToken = require('../middleware/verifyToken')

router.post('/tourism', verifyToken, async (req, res) => {
    return TourismService.create(res, req)
});
router.get('/tourism', verifyToken, async (req, res) => {
    return TourismService.list(res, req)
});

router.post('/tourism/setFavorite', verifyToken, async (req, res) => {
    return TourismService.setFavorite(res, req)
});
router.patch('/tourism/:id', verifyToken, async (req, res) => {
    const { id } = req.params;
    return TourismService.update(res, req, id)
});

router.delete('/tourism/:id', verifyToken, async (req, res) => {
    const { id } = req.params;
    return TourismService.delete(res, id)
});

module.exports = router;
