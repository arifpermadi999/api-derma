// login.route.js

const express = require('express');
const LoginService = require('../service/login.service')
const router = express.Router();
// Login route
router.post('/login', async (req, res) => {
    const { email, password } = req.body;
    return LoginService.login(res, email, password);
});

module.exports = router;
