// app.js
require('dotenv').config()
const express = require('express');
const sequelize = require('./config/db');
const loginRoute = require('./routes/login.route');
const tourismRoute = require('./routes/tourism.route');
const tourismWebRoute = require('./routes/tourism.web.route');
const userSeed = require('./seed/user.seed');
const tourismSeed = require('./seed/tourism.seed');

const cors = require('cors');
const app = express();
const PORT = process.env.PORT || 3000;
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
const bcrypt = require('bcryptjs');
app.use(express.json());

app.use(loginRoute);
app.use(tourismRoute);
app.use(tourismWebRoute);


// Sync the database and start the server
(async () => {
    try {
        await sequelize.sync();
        console.log('Database synchronized');


        userSeed();
        tourismSeed();


        app.listen(process.env.PORT, () => {
            console.log(`Server is running on http://localhost:${process.env.PORT}`);
        });
    } catch (error) {
        console.error('Error synchronizing database:', error);
    }
})();
