const Tourism = require('../models/tourism.model');
const { Op } = require('sequelize');

class TourismService {
    static async create(res, req) {
        try {
            const user = await Tourism.create(req.body);
            return res.json({ message: "success create tourism" });
        } catch (error) {
            return res.status(500).json({ error: 'Internal server error' });
        }
    }
    static async update(res, req, id) {
        try {
            await Tourism.update(req.body, { where: { id: id } });
            return res.json({ message: "success update tourism" });
        } catch (error) {
            console.error('Error logging in:', error);
            return res.status(500).json({ error: 'Internal server error' });
        }
    }
    static async list(res, req) {
        try {
            const { search } = req.query;
            var tourism;
            if (search) {
                tourism = await Tourism.findAll({ where: { name: { [Op.like]: `%${search}%` } } });
            } else {
                tourism = await Tourism.findAll();
            }
            return res.json(tourism);
        } catch (error) {
            console.error('Error logging in:', error);
            return res.status(500).json({ error: 'Internal server error' });
        }
    }
    static async setFavorite(res, req) {
        try {
            const { id, favorite } = req.body;
            await Tourism.update({ is_favorite: favorite }, { where: { id: id } });
            res.json({ message: "success set favorite" });
        } catch (error) {
            console.error('Error logging in:', error);
            res.status(500).json({ error: 'Internal server error' });
        }
    }
    static async delete(res, id) {
        try {
            await Tourism.destroy({ where: { id: id } });
            return res.json({ message: "success delete tourism" });
        } catch (error) {
            console.error('Error logging in:', error);
            return res.status(500).json({ error: 'Internal server error' });
        }
    }
}

module.exports = TourismService