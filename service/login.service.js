
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User = require('../models/user.model');

class LoginService {
    static async login(res, email, password) {
        try {
            const user = await User.findOne({ where: { email } });
            if (!user) {
                return res.status(404).json({ error: 'User not found' });
            }
            const isValidPassword = await user.isValidPassword(password);
            if (!isValidPassword) {
                return res.status(401).json({ error: 'Invalid password' });
            }
            const token = jwt.sign({ id: user.id, email: user.email }, '12345678', { expiresIn: '9999h' });
            return res.json({ token, user });
        } catch (error) {
            console.error('Error logging in:', error);
            return res.status(500).json({ error: 'Internal server error' });
        }
    }
}


module.exports = LoginService;