// user-seed.js
const Tourism = require('../models/tourism.model');
const User = require('../models/user.model');

const bcrypt = require('bcryptjs');

async function seed() {
    const data = await Tourism.findAll();

    try {

        if (data.length == 0) {
            const tourism = [
                { name: "Taman Mini", image: "https://www.cathaypacific.com/content/dam/destinations/jakarta/cityguide-gallery/jakarta_taman_mini_indonesia_920x500.jpg", latlong: "-6.301761335872941, 106.88972073863842", invitation_text: "Ayo datang ke taman mini", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", },
                {
                    name: "Dufan", image: "https://velindotravel.com/wp-content/uploads/2023/03/dufan-ancol.jpg", latlong: "-6.124608339000519, 106.83332312329804", invitation_text: "Ayo datang ke dufan", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                },
            ];
            //await User.sync({ force: true }); // This drops the table and re-creates it, be careful in production!
            await Tourism.bulkCreate(tourism);
            console.log('Seed data inserted successfully.');
        }
    } catch (error) {
        console.error('Error seeding data:', error);
    } finally {

        if (data.length == 0) {
            await Tourism.sequelize.close(); // Close the database connection when done
        }
    }
}

module.exports = seed;
