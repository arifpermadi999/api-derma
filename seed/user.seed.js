// user-seed.js
const User = require('../models/user.model');

const bcrypt = require('bcryptjs');

async function seed() {
    const user = await User.findAll();

    try {

        if (user.length == 0) {
            const salt = await bcrypt.genSalt(10);
            const pw = await bcrypt.hash("12345678", salt);

            const users = [
                { name: 'arif', email: 'arifpermadi999@gmail.com', password: pw },
                // Add more users as needed
            ];
            //await User.sync({ force: true }); // This drops the table and re-creates it, be careful in production!
            await User.bulkCreate(users);
            console.log('Seed data inserted successfully.');
        }
    } catch (error) {
        console.error('Error seeding data:', error);
    } finally {

        if (user.length == 0) {
            await User.sequelize.close(); // Close the database connection when done
        }
    }
}

module.exports = seed;
